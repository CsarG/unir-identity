const Empresa = require("../../build/contracts/ClaimVerifier");
const Web3 = require("web3");
const Config = require("./config");

const password = Config.password;
const web3 = new Web3(Config.url);

let accountEmpresa = Config.accounts[2].account;
let addressEmpresa = Config.contracts[2].contract;

//Desbloquear cuenta
DesbloquearCuenta(accountEmpresa).then(
  result => console.log(`Cuenta: ${accountEmpresa} desbloqueada: ${result}`),
  error => {
    console.log(error);
  }
);

//Contrato
const contractEmpresa = new web3.eth.Contract(Empresa.abi, addressEmpresa, {
  defaultAccount: accountEmpresa, // default from address
  defaultGasPrice: "20000000000" // default gas price in wei, 20 gwei in this case
});

/**
 * Validar un testimonio de una identidad (estudiante)
 * @param {address de la identidad a validar} identity
 * @param {tipo de testimonio} claimType
 */
exports.checkClaim = async (identity, claimType) => {
  return new Promise((resolve, reject) => {
    contractEmpresa.methods.checkClaim(identity, claimType).send(
      {
        from: accountEmpresa,
        gas: Config.gasLimit,
        gasPrice: web3.eth.defaultGasPrice
      },
      (error, transactionHash) => {
        if (error) {
          reject(error);
        } else {
          Events(contractEmpresa, transactionHash).then(result => {
            resolve(result);
          });
        }
      }
    );
  });
};

/**
 * Retorna la direccion de la entidad de confianza
 */
exports.trustedClaimHolder = async () => {
  return new Promise((resolve, reject) => {
    contractEmpresa.methods
      .trustedClaimHolder()
      .call({ from: accountEmpresa })
      .then(
        result => {
          resolve(result);
        },
        error => {
          reject(error);
        }
      );
  });
};

/**
 * Desbloquear cuentas
 * @param {Cuenta ethereum para desbloquear} account
 */
function DesbloquearCuenta(account) {
  return new Promise((resolve, reject) => {
    web3.eth.personal.unlockAccount(account, password, 6000).then(
      result => {
        resolve(result);
      },
      error => {
        reject(error);
      }
    );
  });
}

/**
 * Metodo para buscar eventos de un contrato por hash
 * @param {Contrato para buscar eventos} myContract
 * @param {Hash de la transaccion} txHash
 */
function Events(myContract, txHash) {
  return new Promise((resolve, reject) => {
    myContract
      .getPastEvents(
        "allEvents",
        {
          fromBlock: 0,
          toBlock: "latest"
        },
        (error, events) => {
          if (error) {
            console.log(error);
          }
        }
      )
      .then(events => {
        events.forEach(event => {
          if (event.transactionHash == txHash) {
            resolve(event.returnValues);
          }
        });
      });
  });
}
