var Empresa = require("./Empresa");
var Estudiante = require("./Estudiante");
var Universidad = require("./Universidad");
const Config = require("./config");

let addressUniversidad = Config.contracts[1].contract;

async function startUniversidad() {
  await Universidad.addKey(3, 1).then(
    result => {
      console.log("-----------------------------------------------------------");
      result[0].then( result => { console.log(result); });
      console.log("Resultado signer: ", result[1]);

      Estudiante.addClaim(3,1,addressUniversidad,'Data OK','cesar.com',result[1]).then(result=>{
        console.log("-----------------------------------------------------------");
        console.log("Resultado addClaim: ", result.executionId.toNumber());
        
        Estudiante.approve(result.executionId.toNumber(), true).then(result=>{
          console.log(result);
        });

      });

      
    },
    error => {
      console.log(error);
    }
  );
}

startUniversidad();