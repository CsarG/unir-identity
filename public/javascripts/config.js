exports.url = "HTTP://127.0.0.1:8545"; //Dev: HTTP://127.0.0.1:8545 Unir: http://138.4.143.82:8545
exports.password = ""; //Dev: "" Unir: Alumnos_2018_Q4_IKx5srvT
exports.gasLimit = 996721975;

exports.accounts = [
  {
    id: 1,
    account: '0xFad7D5B66bC3044dDBB97C2F3D115d8B15d9e8D8',
    unlocked: false
  },
  {
    id: 2,
    account: '0xCc97bF9B4e55f152DF4eBe4105f5126C4F541408',
    unlocked: false
  },
  {
    id: 3,
    account: '0x210E98e7f7DF1b79E3a962d9aF1D78AA71bf5c3C',
    unlocked: false
  },
  {
    id: 4,
    account: '0x036bA86aFBD0Ff966F6b0e216951e2AE686DaC6A',
    unlocked: false
  },
  {
    id: 5,
    account: '0x2566ca153f1Ba03A6A814B03937CE7E1B34C3916',
    unlocked: false
  },
  {
    id: 6,
    account: '0xc98818389aFFabF81d192cb95736A0D4a20788B2',
    unlocked: false
  }
];

exports.contracts = [
  {
    detalle: 'Estudiante',
    contract: '0x7591bc2009700ab77141f6b2d24b7b7ac26d8da5'
  },
  {
    detalle: 'Universidad',
    contract: '0xc2e519c689bb2d2d94996f6d4af1f4e7e0a36c48',
  },
  {
    detalle: 'Empresa',
    contract: '0xc14a38e1cfb2598a9ecf0dff5f9bdebbb4d43847',
  }
];

