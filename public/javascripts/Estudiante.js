const Estudiante = require("../../build/contracts/ClaimHolder");
const Web3 = require("web3");
const Config = require("./config");

const password = Config.password;
const web3 = new Web3(Config.url);

let accountEstudiante = Config.accounts[0].account;
let addressEstudiante = Config.contracts[0].contract;

let accountUniversidad = Config.accounts[1].account;

//Desbloquear cuenta
DesbloquearCuenta(accountEstudiante).then(
  result => console.log(`Cuenta: ${accountEstudiante} desbloqueada: ${result}`),
  error => {
    console.log(error);
  }
);

//Contrato
const contractEstudiante = new web3.eth.Contract(
  Estudiante.abi,
  addressEstudiante,
  {
    defaultAccount: accountEstudiante, // default from address
    defaultGasPrice: "20000000000" // default gas price in wei, 20 gwei in this case
  }
);

/**
 * Agregar llave al contrato de identidad
 * @param {Proposito de la llave 1 Managment/2 Action/3 Claim signer/ 4 Encription} purpose
 * @param {Tipo de la llave 1 ECDSA/2 RSA} type
 */
exports.addKey = async (purpose, type) => {

  let prvSigner = web3.utils.randomHex(32);
  let pubSigner = web3.eth.accounts.privateKeyToAccount(prvSigner).address;
  let key = web3.utils.sha3(pubSigner);

  let eventAddKey = Promise((resolve, reject) => {
    contractUniversidad.methods.addKey(key, purpose, type).send(
      {
        from: accountUniversidad,
        gas: Config.gasLimit,
        gasPrice: web3.eth.defaultGasPrice
      },
      (error, transactionHash) => {
        if (error) {
          reject(error);
        } else {
          Events(contractUniversidad, transactionHash).then(result => {
            resolve(result);
          });
        }
      }
    );
  });

  return [eventAddKey, prvSigner];

};

/**
 * Agregar un testimonio
 * @param {} claimType
 * @param {} scheme
 * @param {} issuer
 * @param {} data
 * @param {} uri
 * @param {} keySigner
 */
exports.addClaim = async (claimType, scheme, issuer, data, uri, keySigner) => {

  let dataHash = web3.utils.asciiToHex(data);
  let executionId;
  let hashed = web3.utils.soliditySha3(
    addressEstudiante,
    claimType,
    dataHash
  );
  let signed = await web3.eth.accounts.sign(hashed, keySigner);

  let studentABI = await contractEstudiante.methods
    .addClaim(
      claimType,
      scheme,
      issuer,
      signed.signature,
      dataHash,
      uri
    )
    .encodeABI();
  return new Promise((resolve, reject) => {
    contractEstudiante.methods.execute(
      addressEstudiante,
      0,
      studentABI
    ).send({
      from: accountUniversidad,
      gas: Config.gasLimit,
      gasPrice: web3.eth.defaultGasPrice
    },
      (error, transactionHash) => {
        if (error) {
          reject(error);
        } else {
          Events(contractEstudiante, transactionHash).then(result => {
            resolve(result);
          });
        }
      });
  });


};

exports.approve = async (id, approve) => {

  return new Promise((resolve, reject) => {
    contractEstudiante.methods.approve(id, approve).send({
      from: accountEstudiante,
      gas: Config.gasLimit,
      gasPrice: web3.eth.defaultGasPrice
    }, (error, transactionHash) => {
      if (error) {
        reject(error);
      } else {
        Events(contractEstudiante, transactionHash).then(result => {
          resolve(result);
        });
      }
    })
  });
};

// execute
// address _to, uint256 _value, bytes _data
// removeClaim
// bytes32 _claimId
// removeKey
// bytes32 _key
// getClaim
// bytes32 _claimId
// getClaimIdsByType
// uint256 _claimType
// getKey
// bytes32 _key
// getKeyPurpose
// bytes32 _key
// getKeysByPurpose
// uint256 _purpose
// keyHasPurpose
// bytes32 _key, uint256 _purpose

/**
 * Desbloquear cuentas
 * @param {Cuenta ethereum para desbloquear} account
 */
function DesbloquearCuenta(account) {
  return new Promise((resolve, reject) => {
    web3.eth.personal.unlockAccount(account, password, 6000).then(
      result => {
        resolve(result);
      },
      error => {
        reject(error);
      }
    );
  });
}

/**
 * Metodo para buscar eventos de un contrato por hash
 * @param {Contrato para buscar eventos} myContract
 * @param {Hash de la transaccion} txHash
 */
function Events(myContract, txHash) {
  return new Promise((resolve, reject) => {
    myContract
      .getPastEvents(
        "allEvents",
        {
          fromBlock: 0,
          toBlock: "latest"
        },
        (error, events) => {
          if (error) {
            console.log(error);
          }
        }
      )
      .then(events => {
        events.forEach(event => {
          if (event.transactionHash == txHash) {
            resolve(event.returnValues);
          }
        });
      });
  });
}
