const Web3 = require('web3');
const config = require('../../config/config');
const Tx = require('ethereumjs-tx');

const web3 = new Web3(config.urlLocal);

/**Obtener la clave publica desde la privada */
exports.getPrivateKey = async function(){
    let prvSigner =config.PRIVATE_KEY_1;

    let pubSigner = web3.eth.accounts.privateKeyToAccount(prvSigner).address;

    var key = web3.utils.sha3(pubSigner)

    return key;
}

/**Obtener las cuentas de la red Blockchain */
exports.getAccounts = function () {
    return web3.eth.getAccounts();
};

/**Desbloquear una cuenta de la red Blockchain 
 * @account{address}
 * @password{string}
*/
exports.unLockAccount = function (account, password) {
    web3.eth.personal.unlockAccount(account, password, 600).then(console.log('Account unlocked: ', account));
}

/**Inicializa un smart contract en la red Blockchain
 * @abi{object}
 * @address{address}
 */
exports.newContract = function (abi, address) {
    return new web3.eth.Contract(abi, address, {});
}

/**Agregar llave a un contrato
 * @contract{smart contract}
 * @account{address}
 */
exports.addKeyClaim = async function (contract, account, keytoon) {
    try {

        let purpose = 3;
        let keyType = 1;

        //let key = web3.utils.sha3(pubSigner);

        var key = web3.utils.sha3(keytoon);

        var acctSha3 = web3.utils.keccak256(account)

        return contract.methods.addKey(keytoon, 3, 1).send({
            from: account, gasLimit: '6721975', gasPrice: '0'
          });
    }
    catch (exception) {
        console.log('exception addkey: ', exception);
    }
}

/**Agregar un testimonio
 * @contract{smart contract}
 * @addressContract{address}
 * @account{address}
 */
exports.addClaim = async function (contract, addressContract, account, prvSigner) {
    let claimType = 3;
    let scheme = 1;
    let issuer = addressContract; //address
    let signed;
    let data = web3.utils.asciiToHex('Verified OK'); //optional
    let uri; //optional

    let hashed = web3.utils.soliditySha3(addressContract, claimType, data);
    signed = await web3.eth.accounts.sign(hashed, prvSigner);

    let claimRes = await contract.methods
        .addClaim(
            claimType,
            2,
            addressContract,
            signed.signature,
            data,
            'unir.com'
        ).send({ from: account, gasLimit: '6721975', gasPrice: '0' });

    return claimRes.events.ClaimAdded;
}

/**Desplegar un contrato en la red Blockchain
 * @contract{Smart Contract}
 * @account{address}
 */
exports.deployClaimHolder = async function (contract, account) {
    var Contract = new web3.eth.Contract(contract.abi)
    var tx = Contract.deploy({
        data: web3.utils.toHex(contract.data),
        arguments: []
    }).send({ gas: 4612388, from: account })
    return tx;
}

/**Desplegar un contrato verificador en la red Blockchain 
 * @contract{smart contract}
 * @account{address}
 * @args{string}
*/
exports.deployClaimVerifier = async function(contract,account,args){
    var Contract = new web3.eth.Contract(contract.abi)
    var tx = Contract.deploy({
      data: web3.utils.toHex(contract.data),
      arguments: [args]
    }).send({ gas: 4612388, from: account })
    return tx;
}

/**Verificar un testimonio
 * @contratoVerifier{smart contract}
 * @addressContract{address}
 * @account{address}
 * @typeClaim{tipo de claim}
 */
exports.checkClaim = async function (contratoVerifier, addressContract, account, typeClaim) {
    let res = await contratoVerifier.methods
        .checkClaim(addressContract, type)
        .send({ from: account })

    console.log(res);
}