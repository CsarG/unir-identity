let truffleAssert = require("truffle-assertions");
let ClaimHolder = artifacts.require("ClaimHolder");
let ClaimVerifier = artifacts.require("ClaimVerifier");
const JsonClaimHolder = require("../build/contracts/ClaimHolder");

const Web3 = require("web3");
const config = require("../public/javascripts/config");

const web3 = new Web3(config.url);

contract("ClaimHolder", async accounts => {
  //Variables generales
  let instanceStudent;
  let instanceUniversity;
  let instanceCompany;
  let prvSigner;
  let pubSigner;
  let ContractStudent;

  before(async () => {
    instanceStudent = await ClaimHolder.at(config.contracts[0].contract);
    instanceUniversity = await ClaimHolder.at(config.contracts[1].contract);
    instanceCompany = await ClaimVerifier.at(config.contracts[2].contract);

    ContractStudent = new web3.eth.Contract(
      JsonClaimHolder.abi,
      instanceStudent.address
    );

    prvSigner = web3.utils.randomHex(32);
    pubSigner = web3.eth.accounts.privateKeyToAccount(prvSigner).address;

    accountStudent = accounts[0];
    accountUniversity = accounts[1];
    accountCompany = accounts[2];
  });

  it("Test 1: Agregar key proposito 3 en contrato identidad universidad", async () => {
    let key = web3.utils.sha3(pubSigner);
    let transfer = await instanceUniversity.addKey(key, 3, 1, {
      from: accountUniversity
    });
    truffleAssert.eventEmitted(transfer, "KeyAdded", result => {
      return (
        result.key === key &&
        result.purpose.toNumber() === 3 &&
        result.keyType.toNumber() === 1
      );
    });
  });

  it("Test 2: Agregar claim en contrato identidad estudiante, ejecutar y aprobar", async () => {
    let data = web3.utils.asciiToHex("Verified OK");
    let claimType = 3;
    let executionId;
    let hashed = web3.utils.soliditySha3(
      instanceStudent.address,
      claimType,
      data
    );
    let signed = await web3.eth.accounts.sign(hashed, prvSigner);

    let studentABI = await ContractStudent.methods
      .addClaim(
        claimType,
        1,
        instanceUniversity.address,
        signed.signature,
        data,
        "unir.com"
      )
      .encodeABI();
    let transfer = await instanceStudent.execute(
      instanceStudent.address,
      0,
      studentABI,
      { from: accountUniversity }
    );

    truffleAssert.eventEmitted(transfer, "ExecutionRequested", async result => {
      executionId = result.executionId.toNumber();
      let approve = await instanceStudent.approve(executionId, true, {
        from: accountStudent
      });
      truffleAssert.eventEmitted(approve, "Approved", result => {
        return result.executionId.toNumber() > 0 && result.approved;
      });
      truffleAssert.eventEmitted(approve, "ClaimAdded", result => {
        return (
          result.claimType == 3 &&
          result.scheme == 1 &&
          result.issuer == instanceUniversity.address &&
          result.signature == signed.signature &&
          result.data == data
        );
      });
      truffleAssert.eventEmitted(approve, "Executed", result => {
        return (
          result.executionId.toNumber() == executionId &&
          result.to == instanceStudent.address
        );
      });
    });
  });

  it("Test 8: Validar la identidad del estudiante desde la empresa", async () => {
    let transfer = await instanceCompany.checkClaim(
      instanceStudent.address,
      3,
      { from: accountCompany }
    );
    truffleAssert.eventEmitted(transfer, "ClaimValid", result => {
      return (
        result._identity == instanceStudent.address &&
        result.claimType.toNumber() == 3
      );
    });
  });
});
