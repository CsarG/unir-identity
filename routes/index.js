var express = require('express');
var router = express.Router();
var dataAccess = require('../public/javascripts/dataAccess')
var web3Service = require('../public/javascripts/web3Service')

/* GET home page. */
var personas = [
    {
        id: 1,
        nombre: "MitoCode"
    },
    {
        id: 2,
        nombre: "Mito"
    },
    {
        id: 3,
        nombre: "Code"
    }
]

//data.accesoAccounts();

router.get('/', (req, res) => {

    web3Service.getAccounts().then((resultado) => {
        res.render(
            'index',
            { titulo: 'Handlebars', mensaje: `MitoCode | ${resultado[0]}`, personas: personas })
    }, (error) => {
        console.log(error);
    });


});

module.exports = router;
