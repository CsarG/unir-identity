const ClaimHolderStudent = artifacts.require("ClaimHolder");
const ClaimHolderUniversity = artifacts.require("ClaimHolder");
const ClaimVerifier = artifacts.require("ClaimVerifier");
const ERC725 = artifacts.require("ERC725");
const ERC735 = artifacts.require("ERC735");
const Identity = artifacts.require("Identity");
const KeyHolder = artifacts.require("KeyHolder");

module.exports = function(deployer) {
  deployer.deploy(ERC725);
  deployer.deploy(ERC735);
  deployer.deploy(Identity);
  deployer.deploy(KeyHolder);
};

module.exports = function(deployer, network, accounts) {
  deployer
    .deploy(ClaimHolderStudent, { from: accounts[0] })
    .then(() => {
      return deployer.deploy(ClaimHolderUniversity, { from: accounts[1] });
    })
    .then(() => {
      return deployer.deploy(ClaimVerifier, ClaimHolderUniversity.address, {
        from: accounts[2]
      });
    });
};
